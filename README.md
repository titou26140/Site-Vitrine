# Site-Vitrine
This is a responsive web design for freelance developper.

I used :  
-> **BOWER** for packet management  
-> **BOOTSTRAP** 4.0 css framework  
-> **HTML / CSS**  

**Screenshot :**

![alt text](https://image.ibb.co/cpkrow/screencapture_file_Users_timothe_Sites_Site_Vitrine_index_html_1507276132373_copie_1.jpg)  

**Usage**  
gulp serve  
gulp dist  
gulp sass  
gulp clear  

**Contact :**  
contact@timotheduc.com
