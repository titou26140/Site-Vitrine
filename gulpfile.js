var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var browserSync = require('browser-sync').create();
var htmlmin = require('gulp-html-minifier');
var clean = require('gulp-clean');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');
const image = require('gulp-image');



gulp.task('dist', ['sass', 'image'], function() {
  gulp.src('./dev/index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist'));
  gulp.src(['./dev/styles/css/**/*'])
    .pipe(gulp.dest('./dist/styles/css/'));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./dev/"
        }
    });
});

gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./dev/"
    });

    gulp.watch("dev/styles/sass/*.scss", ['sass']);
    gulp.watch("dev/**/*.html").on('change', browserSync.reload);
    gulp.watch("dev/styles/css/*.css").on('change', browserSync.reload);
});

gulp.task('sass', function() {
    return gulp.src("./dev/styles/sass/*.scss")
        .pipe(sass())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest("./dev/styles/css"))
        .pipe(browserSync.stream());
});

gulp.task('clear', function() {
    return gulp.src('dist/', {read: false})
        .pipe(clean());
});

gulp.task('image', function () {
    gulp.src('./dev/medias/**/*.{png,jpg}')
        .pipe(image())
        .pipe(gulp.dest('./dist/medias/'));
});

gulp.task('deploy', function () {

    var conn = ftp.create({
        host: 'ftp.cluster020.hosting.ovh.net',
        user: 'timothedgp',
        password: 'Timotheduc26140',
        parallel: 10,
        log: gutil.log
    });

    var globs = [
        'dist/**'
    ];

    // using base = '.' will transfer everything to /public_html correctly 
    // turn off buffering in gulp.src for best performance 

    return gulp.src(globs, {
            base: './dist',
            buffer: false
        })
        .pipe(conn.newer('/www')) // only upload newer files 
        .pipe(conn.dest('/www'));

});

gulp.task('default', ['serve'], function() {
});
